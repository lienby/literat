﻿namespace LiteRATCommander
{
    partial class LightRATCommander
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Users = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UserName = new System.Windows.Forms.TextBox();
            this.ChangeName = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.CommandsBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.EnterCommand = new System.Windows.Forms.TextBox();
            this.SndCommand = new System.Windows.Forms.Button();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Users
            // 
            this.Users.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Users.FormattingEnabled = true;
            this.Users.Location = new System.Drawing.Point(12, 27);
            this.Users.Name = "Users";
            this.Users.Size = new System.Drawing.Size(131, 316);
            this.Users.TabIndex = 0;
            this.Users.SelectedIndexChanged += new System.EventHandler(this.Users_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Users:";
            // 
            // UserName
            // 
            this.UserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserName.Location = new System.Drawing.Point(213, 27);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(438, 20);
            this.UserName.TabIndex = 2;
            this.UserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.UserName_KeyPress);
            // 
            // ChangeName
            // 
            this.ChangeName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangeName.Location = new System.Drawing.Point(657, 24);
            this.ChangeName.Name = "ChangeName";
            this.ChangeName.Size = new System.Drawing.Size(121, 23);
            this.ChangeName.TabIndex = 3;
            this.ChangeName.Text = "Change Name";
            this.ChangeName.UseVisualStyleBackColor = true;
            this.ChangeName.Click += new System.EventHandler(this.ChangeName_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "User Name";
            // 
            // CommandsBox
            // 
            this.CommandsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandsBox.Location = new System.Drawing.Point(152, 53);
            this.CommandsBox.Multiline = true;
            this.CommandsBox.Name = "CommandsBox";
            this.CommandsBox.ReadOnly = true;
            this.CommandsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CommandsBox.Size = new System.Drawing.Size(626, 302);
            this.CommandsBox.TabIndex = 5;
            this.CommandsBox.TextChanged += new System.EventHandler(this.CommandsBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(148, 358);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Enter Command:";
            // 
            // EnterCommand
            // 
            this.EnterCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EnterCommand.Location = new System.Drawing.Point(239, 355);
            this.EnterCommand.Name = "EnterCommand";
            this.EnterCommand.Size = new System.Drawing.Size(412, 20);
            this.EnterCommand.TabIndex = 7;
            this.EnterCommand.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterCommand_KeyPress);
            // 
            // SndCommand
            // 
            this.SndCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SndCommand.Location = new System.Drawing.Point(657, 353);
            this.SndCommand.Name = "SndCommand";
            this.SndCommand.Size = new System.Drawing.Size(121, 23);
            this.SndCommand.TabIndex = 8;
            this.SndCommand.Text = "Send Command";
            this.SndCommand.UseVisualStyleBackColor = true;
            this.SndCommand.Click += new System.EventHandler(this.SndCommand_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RefreshButton.Location = new System.Drawing.Point(12, 352);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(130, 23);
            this.RefreshButton.TabIndex = 9;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // LightRATCommander
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 380);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.SndCommand);
            this.Controls.Add(this.EnterCommand);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CommandsBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChangeName);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Users);
            this.Name = "LightRATCommander";
            this.Text = "LiteRAT Commander";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LightRatCommander_Closing);
            this.Load += new System.EventHandler(this.LightRatCommander_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Users;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.Button ChangeName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CommandsBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox EnterCommand;
        private System.Windows.Forms.Button SndCommand;
        private System.Windows.Forms.Button RefreshButton;
    }
}

